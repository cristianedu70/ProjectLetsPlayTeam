﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectLetsPlayTeam2.Domain
{
    public class Usuarios
    {
        [Key]//asigno la primary key de la bd
        public int UserId { get; set; }

        [Required(ErrorMessage = "The field {0} is required.")]//Valido que el campo sea requerido
        [MaxLength(50, ErrorMessage = "The field {0} only can contain {1} characters lenght")]
        [Index("Usuarios_UserName_Index", IsUnique = true)]//genero el index
        public string UserName { get; set; }

        [Required(ErrorMessage = "The field {0} is required.")]
        [DataType (DataType.Password)]
        public string Pass { get; set; }
    }
}
