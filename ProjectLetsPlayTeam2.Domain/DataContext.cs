﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectLetsPlayTeam2.Domain
{
    public class DataContext : DbContext //una clase publica que hereda del DbContext, librería de entity framework para conectar a bd
    {
        public DataContext() : base("DefaultConnection") //cada vez que usemos la clase context se conectará a la bd.
            //Si la bd no existe, la crea, si la tabla no existe, la crea
        {

        }
        public System.Data.Entity.DbSet<ProjectLetsPlayTeam2.Domain.Usuarios> Usuarios { get; set; }
    }
}
